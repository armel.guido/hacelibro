#!/usr/bin/bash

## usage: ./hacelibro path_to_book.pdf scale. Where scale go from 0.7 to 1.1.

pdf2ps $1 book.ps;

psbook -s4 book.ps foo_book.ps;

rm book.ps;

psnup -s$2 -2 foo_book.ps booklet.ps;

rm foo_book.ps;

ps2pdf booklet.ps;

rm booklet.ps;
